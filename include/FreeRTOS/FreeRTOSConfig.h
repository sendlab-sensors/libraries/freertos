/**
 * All documentation
 * @author Patrick de Jong
 * @date 16/04/2020
 */
#ifndef FREERTOS_CONFIG_H_
#define FREERTOS_CONFIG_H_


/**
 * Pre-emptive scheduling algorithms will immediately ‘pre-empt’ the Running
 * state task if a task that has a priority higher than the Running state task
 * enters the Ready state. Being pre-empted means being involuntarily (without
 * explicitly yielding or blocking) moved out of the Running state and into the
 * Ready state to allow a different task to enter the Running state.
 */
#define configUSE_PREEMPTION						1 // Chapter 3.12 FreeRTOS Book
#define configUSE_PORT_OPTIMISED_TASK_SELECTION		0
#define configUSE_TICKLESS_IDLE						0 // We cannot run this since it's not ported for the MCU Atmega324PB-AU
#define configCPU_CLOCK_HZ							((unsigned long) 20000000)

/**
 * The tick interrupt is used to measure time. Therefore a higher tick frequency
 * means time can be measured to a higher resolution. However, a high tick frequency
 * also means that the RTOS kernel will use more CPU time so be less efficient.
 * The RTOS demo applications all use a tick rate of 1000Hz.
 */
#define configTICK_RATE_HZ                          1000

/**
 * The number of priorities available to the application tasks.
 * Any number of tasks can share the same priority.
 */
#define configMAX_PRIORITIES					    4

/**
 * The maximum permissible length of the descriptive name given to a
 * task when the task is created. The length is specified in the number
 * of characters including the NULL termination byte.
 */
#define configMAX_TASK_NAME_LEN                     16

#define configUSE_16_BIT_TICKS		                1 // https://www.freertos.org/a00110.html#configUSE_16_BIT_TICKS
#define configIDLE_SHOULD_YIELD		                1
#define configUSE_TASK_NOTIFICATIONS                1 // https://www.freertos.org/RTOS-task-notifications.html

/*
 * For the boardsupport Semaphores will be used:
 * https://www.tutorialspoint.com/mutex-vs-semaphore
 */
#define configUSE_MUTEXES                           0
#define configUSE_RECURSIVE_MUTEXES                 0
#define configUSE_COUNTING_SEMAPHORES               0
#define configQUEUE_REGISTRY_SIZE                   0
#define configUSE_QUEUE_SETS                        1
#define configUSE_TIME_SLICING                      1
#define configUSE_NEWLIB_REENTRANT                  0
#define configENABLE_BACKWARD_COMPATIBILITY         0
#define configNUM_THREAD_LOCAL_STORAGE_POINTERS     5 // https://www.freertos.org/thread-local-storage-pointers.html
#define configSTACK_DEPTH_TYPE                      uint16_t
#define configMESSAGE_BUFFER_LENGTH_TYPE            size_t

/* Memory allocation related definitions. */
#define configSUPPORT_STATIC_ALLOCATION				0
#define configMINIMAL_STACK_SIZE	                ((unsigned short) 128)
#define configSUPPORT_DYNAMIC_ALLOCATION      	    1
#define configTOTAL_HEAP_SIZE		                ((size_t) (1500 ))
#define configAPPLICATION_ALLOCATED_HEAP		    0


/* Hook function related definitions. */
#define configUSE_IDLE_HOOK                         0 // https://www.freertos.org/RTOS-idle-task.html
#define configUSE_TICK_HOOK                         0
#define configCHECK_FOR_STACK_OVERFLOW              0
#define configUSE_MALLOC_FAILED_HOOK                0
#define configUSE_DAEMON_TASK_STARTUP_HOOK          0


/* Run time and task stats gathering related definitions. */
#define configGENERATE_RUN_TIME_STATS               0
#define configUSE_TRACE_FACILITY                    0
#define configUSE_STATS_FORMATTING_FUNCTIONS        0

/* Co-routine related definitions. */
#define configUSE_CO_ROUTINES                       1
#define configMAX_CO_ROUTINE_PRIORITIES             2

/* Software timer related definitions. */
#define configUSE_TIMERS                            0 // INCLUDE SOFTWARE TIMERS
#define configTIMER_TASK_PRIORITY                   4
#define configTIMER_QUEUE_LENGTH                    10
#define configTIMER_TASK_STACK_DEPTH                configMINIMAL_STACK_SIZE


/* Interrupt nesting behaviour configuration. */
/**
 * Not Available for our board
 */
#define configKERNEL_INTERRUPT_PRIORITY             0
#define configMAX_SYSCALL_INTERRUPT_PRIORITY1       0
#define configMAX_API_CALL_INTERRUPT_PRIORITY       0

/* Optional functions - most linkers will remove unused functions anyway. */
#define INCLUDE_vTaskPrioritySet                    1
#define INCLUDE_uxTaskPriorityGet                   1
#define INCLUDE_vTaskDelete                         1
#define INCLUDE_vTaskSuspend                        1
#define INCLUDE_xResumeFromISR                      0
#define INCLUDE_vTaskDelayUntil                     1
#define INCLUDE_vTaskDelay                          1
#define INCLUDE_xTaskGetSchedulerState              1
#define INCLUDE_xTaskGetCurrentTaskHandle           1
#define INCLUDE_uxTaskGetStackHighWaterMark         0
#define INCLUDE_xTaskGetIdleTaskHandle              0
#define INCLUDE_eTaskGetState                       1
#define INCLUDE_xEventGroupSetBitFromISR            0
#define INCLUDE_xTimerPendFunctionCall              0
#define INCLUDE_xTaskAbortDelay                     1
#define INCLUDE_xTaskGetHandle                      1
#define INCLUDE_xTaskResumeFromISR                  1

#endif //FREERTOS_CONFIG_H_
